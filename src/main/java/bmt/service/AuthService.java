package bmt.service;

public class AuthService {
    // Replace with your actual username and password
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "password";

    public boolean authenticate(String username, String password) {
        return USERNAME.equals(username) && PASSWORD.equals(password);
    }
}
