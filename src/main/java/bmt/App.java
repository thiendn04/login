package bmt;

import bmt.service.AuthService;

public class App {
    public static void main(String[] args) {
        String username = "admin"; // Tên đăng nhập cố định
        String password = "password"; // Mật khẩu cố định

        AuthService authService = new AuthService();
        boolean isAuthenticated = authService.authenticate(username, password);

        if (isAuthenticated) {
            System.out.println("Login successful!");
        } else {
            System.out.println("Invalid username or password. Login failed.");
        }
    }
}
